import React, { useState } from 'react'
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import {makeStyles} from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Login from '../Components/Login'
import Registration from '../Components/Registration';
const SignInOutContainer=()=>{
const classes = useStyles()
const [value,setValue]=useState(0)
const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const paperStyle={width:400,height:'70vh',margin:"0px auto"}
  function TabPanel(props) {
    const { children, value, index, ...other } = props;
  
    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    );
  }
  
    return (
        <Paper elevation={20} style={paperStyle}>
        <Tabs
          value={value}
          indicatorColor="primary"
          textColor="primary"
          onChange={handleChange}
          classes={{indicator:classes.customStyleOnActiveTab}}
          aria-label="disabled tabs example"
        >
          <Tab label={<span className={ value === 0 ? classes.activeTab : classes.customStyleOnTab}>Sign In</span>} />
         
          <Tab label={<span className={ value === 1 ? classes.activeTab : classes.customStyleOnTab}>Sign Up</span>}/>
        </Tabs>
        <TabPanel value={value} index={0}>
     <Login handleChange={handleChange}/>
   </TabPanel>
   <TabPanel value={value} index={1}>
     <Registration/>
   </TabPanel>
      </Paper>
      
    )
}
const useStyles = makeStyles({
  customStyleOnTab:{
    fontSize:'13px',
    color:'light blue'
  },
  customStyleOnActiveTab:{
    color:'light blue'
  },
  activeTab:{
    fontSize:'13px',
    fontWeight:'600',
    color:'light blue',
  }
})

export default SignInOutContainer;