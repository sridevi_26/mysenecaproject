import './App.css';
import SignInOutContainer from './containers';
import { BrowserRouter } from 'react-router-dom';
import background from "C:/Users/ACER/my-first-project/src/library.jpg";
import {Grid} from '@material-ui/core'
function App() {
  return (
    <BrowserRouter>
    <div style={{backgroundImage: `url(${background})`,
        backgroundPosition:'center',
        backgroundSize:'cover',
        backgroundRepeat:'no-repeat',
        width:'100vw',
        height:'100vh',
        margin:'0px auto'
     }}>
     <Grid
  container
  spacing={0}
  direction="column"
  alignItems="center"
  justify="center"
  style={{ minHeight: '100vh' }}
>

  <Grid item xs={3}>
    <SignInOutContainer/>
  </Grid>   

</Grid>
    </div>
    </BrowserRouter>
  );
}

export default App;