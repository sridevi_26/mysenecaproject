import {Grid,Paper,Avatar} from '@material-ui/core';
import AddCircleOutlineOutlinedIcon from '@material-ui/icons/AddCircleOutlineOutlined';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import Checkbox from '@material-ui/core/Checkbox';
import { Link } from "react-router-dom"
import { makeStyles } from '@material-ui/core/styles';
import { Formik, Field, Form, ErrorMessage } from 'formik'
import { FormHelperText,Button} from '@material-ui/core'
import * as Yup from 'yup'
const Registration=()=>{
    const paperStyle={padding :15,height:'70vh',width:400,margin:"0px auto"}
    const avatarStyle = { backgroundColor: '#1bbd7e' }
    const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/
    const btnstyle = { margin: '8px 0'}
    const initialValues = {
        fname: "",
        lname: "",
        email: "",
        gender: "",
        phoneNumber: "",
        password: "",
        confirmPassword: "",
        termsAndConditions: false
    }
    const validationSchema = Yup.object().shape({
        fname: Yup.string().min(3, "It's too short").required("Required"),
        lname: Yup.string().min(3, "It's too short").required("Required"),
        email: Yup.string().email("Enter valid email").required("Required"),
        gender: Yup.string().oneOf(["male", "female"], "Required").required("Required"),
        phoneNumber: Yup.string().matches(phoneRegExp, 'Phone number is not valid').min(10, "to short")
        .max(10, "to long").required("Required"),
        password: Yup.string().min(8, "Password minimum length should be 8").required("Required"),
        confirmPassword: Yup.string().oneOf([Yup.ref('password')], "Password not matched").required("Required"),
        termsAndConditions: Yup.string().oneOf(["true"], "Accept terms & conditions").required("Required"),
    })
    const onSubmit = (values, props) => {
        console.log(values)
        console.log(props)
        setTimeout(() => {

            props.resetForm()
            props.setSubmitting(false)
        }, 2000)
    }
    return(
        <Grid>
            <Paper style={paperStyle}>
                <Grid align="center">
                <Avatar style={avatarStyle}>
                        <AddCircleOutlineOutlinedIcon />
                    </Avatar>
                <font size="+1">Sign Up</font>
                </Grid>
            <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={onSubmit}>
             {(props) => (
                        <form autoComplete="off" onSubmit={props.handleSubmit}>
                <div class="form-field">
                    <div class="form-group">
                        <label htmlFor="FirstName">FirstName</label>
                        <input 
                              type="text" 
                              placeholder="Enter FirstName" 
                              id="Fname"
                              value={props.values.fname}
                              name="fname" 
                              onChange={props.handleChange}
                               onBlur={props.handleBlur}
                               className={props.errors.fname && props.touched.fname && "error"}
                        />
                         {
                       props.errors.fname && props.touched.fname && (
                           <div className="input-feedback1">{props.errors.fname}</div>
                       )
                         }
                    </div>
                    <div class="form-group">
                        <label htmlFor="LastName">LastName</label>
                        <input  
                             type="text" 
                             placeholder="Enter LastName" 
                             id="Lname"
                             value={props.values.lname}
                              name="lname" 
                              onChange={props.handleChange}
                               onBlur={props.handleBlur}
                              className={props.errors.lname && props.touched.lname && "error"}
                        />
                            {
                       props.errors.lname && props.touched.lname && (
                           <div className="input-feedback1">{props.errors.lname}</div>
                       )
                         }
                    </div>
                   </div>
                
                   <FormControl>
                        <FormLabel>Gender</FormLabel>
                        <RadioGroup aria-label="gender" class="radioButton" name="gender" style={{ display: 'flex' }} value={props.values.gender}
                              onChange={props.handleChange}
                               onBlur={props.handleBlur}
                              className={props.errors.gender && props.touched.gender && "error"}>
                            <FormControlLabel value="female" control={<Radio color="primary"/>} label={<span  style={{ fontSize: '1.2rem' }}>Female</span>}/>
                            <FormControlLabel value="male" control={<Radio color="primary"/>} label={<span  style={{ fontSize: '1.2rem' }}>Male</span>}/>
                        </RadioGroup>
                    </FormControl>
                    {
                       props.errors.gender && props.touched.gender && (
                           <div className="input-feedback2">{props.errors.gender}</div>
                       )
                         }
                       <label>MobileNumber</label>
                        <input 
                           type="text" 
                           placeholder="Enter MobileNumber"
                           name="phoneNumber"
                           onChange={props.handleChange}
                           onBlur={props.handleBlur}
                           className={props.errors.phoneNumber && props.touched.phoneNumber && "error"}
                        />
                        {
                       props.errors.phoneNumber && props.touched.phoneNumber && (
                           <div className="input-feedback1">{props.errors.phoneNumber}</div>
                       )
                         }
                        <label htmlFor="email">EmailID</label>
                        <input 
                             type="text" 
                             placeholder="Enter Email Id"
                             value={props.values.email}
                              name="email" 
                              onChange={props.handleChange}
                               onBlur={props.handleBlur}
                          className={props.errors.email && props.touched.email && "error"}
                          />
                            {
                       props.errors.email && props.touched.email && (
                           <div className="input-feedback1">{props.errors.email}</div>
                       )
                         }
                        <label htmlFor="password">Password</label>
                        <input 
                             type="password" 
                              placeholder="Enter Password"
                              value={props.values.password}
                              name="password" 
                              onChange={props.handleChange}
                              onBlur={props.handleBlur}
                          className={props.errors.password && props.touched.password && "error"}
                          />
                            {
                       props.errors.password && props.touched.password && (
                           <div className="input-feedback1">{props.errors.password}</div>
                       )
                         }
                        <label htmlFor="confirmPassword">Confirm Password</label>
                        <input 
                             type="password" 
                             placeholder="Retype Password"
                             value={props.values.confirmPassword}
                              name="confirmPassword" 
                              onChange={props.handleChange}
                              onBlur={props.handleBlur}
                          className={props.errors.confirmPassword && props.touched.confirmPassword && "error"}
                          />
                            {
                       props.errors.confirmPassword && props.touched.confirmPassword && (
                           <div className="input-feedback1">{props.errors.confirmPassword}</div>
                       )
                         }
                    
                    <FormControlLabel
                      
                        label={<span style={{ fontSize: '1.3rem' }}>I accept the terms and conditions</span>}
                        control={<Checkbox 
                            name="termsAndConditions" 
                            color="primary"
                            value={props.values.termsAndConditions} 
                            onChange={props.handleChange}
                            onBlur={props.handleBlur}
                            className={props.errors.termsAndConditions && props.touched.termsAndConditions && "error"}
                        />}
                        
                    />
                    {
                       props.errors.termsAndConditions && props.touched.termsAndConditions && (
                           <div className="input-feedback4">{props.errors.termsAndConditions}</div>
                       )
                         }
                     <Button type='submit' className="btn btn-primary" color='primary' variant="contained" disabled={props.isSubmitting}
                                style={btnstyle} fullWidth>{props.isSubmitting ? "Loading" : <h6>Sign Up</h6 >}</Button>
                        
                    
                </form>
            )}
            </Formik>
            </Paper>
            
            </Grid>
        );
}
export default Registration;