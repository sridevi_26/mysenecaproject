import {Grid,Paper,Typography,Avatar,Button} from '@material-ui/core';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import { Link } from "react-router-dom";
import {Formik,Field} from 'formik';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import * as Yup from 'yup';
import React from 'react';
const Login=({handleChange})=>{
    const paperStyle={padding :50,height:'70vh',width:400,margin:"0px auto"}
    const avatarStyle={backgroundColor:'#1bbd7e'}
    const btnstyle = { margin: '8px 0'     }
    const initialValues = {
        email: "",
        password: "",
        remember: false
    }
    const validationSchema = Yup.object().shape({
        email:Yup.string()
        .email()
        .required("Required"),

        password:Yup.string()
        .required("No Password Provided")
        .min(8,"Password is too short -should be eight character long")
        .matches(/(?=.*[0-9])/,"Password should contain a number")
    })
    const onSubmit = (values, props) => {
        
        setTimeout(() => {
            props.resetForm() 
            props.setSubmitting(false)
            console.log("Logging in",values)
        }, 1000);

    }
    return(
        <Grid>
           <Paper style={paperStyle}>
                <Grid align='center'>
                     <Avatar style={avatarStyle}><LockOutlinedIcon/></Avatar>
                     <font size="+1">Sign In</font>
                </Grid>
                <Formik initialValues={initialValues} onSubmit={onSubmit} validationSchema={validationSchema}>
                    {(props)=>(
                     <form autoComplete="off" onSubmit={props.handleSubmit}>
                    <label htmlFor="email">EmailID</label>
                    <input 
                    type="text" 
                    value={props.values.email}
                   name="email" 
                   onChange={props.handleChange}
                   onBlur={props.handleBlur}
                    placeholder="Enter Email"
                   className={props.errors.email && props.touched.email && "error"}
                    />
                    {
                       props.errors.email && props.touched.email && (
                           <div className="input-feedback">{props.errors.email}</div>
                       )
                   }
                    <label htmlFor="password">Password</label>
                    <input type="password" 
                     value={props.values.password}
                     name="password" 
                     onChange={props.handleChange}
                     onBlur={props.handleBlur}
                    placeholder="Enter Password"
                    className={props.errors.password && props.touched.password && "error"}
                    />
                     {
                       props.errors.password && props.touched.password && (
                           <div className="input-feedback">{props.errors.password}</div>
                       )
                     }
                     <Field as={FormControlLabel}
                                name='remember'
                                control={
                                    <Checkbox
                                        color="primary"
                                    />
                                }
                                label="Remember me"
                    />
                <Button type='submit' className="btn btn-primary" color='primary' variant="contained" disabled={props.isSubmitting}
                                style={btnstyle} fullWidth>{props.isSubmitting ? "Loading" : <h6>Sign In</h6 >}</Button>
                    </form>
                    )}
                    </Formik>
                    <Typography variant="h6">
                        <Link href="#">
                            Forgot password?
                        </Link>
                    </Typography>
                    <Typography variant="h6">Do you have an account?<Link href='#' onClick={()=>handleChange("event",1)}><h6>Sign Up</h6>
                        </Link>
                    </Typography>
            </Paper>
            
        </Grid>
        
    )
}
export default Login;